from bicycle_hire.extensions import db
from bicycle_hire.blueprints.bicycle.models import Booking, Location, Bicycle
import random, datetime


class Payment(db.Model):
    __tablename__ = 'payments'

    # Table Attributes
    id = db.Column(db.Integer, primary_key = True)
    booking = db.Column(db.Integer,
                        db.ForeignKey('bookings.id'),
                        nullable = False)

    payment_amount = db.Column(db.Float, nullable = False)
    paid = db.Column(db.Boolean, nullable = False)
    time_created = db.Column(db.DateTime, nullable = False)

    def get_payment_amount(self):
        return self.payment_amount

    def get_booking_id(self):
        return self.booking

    # Creates a record in the Payment table
    def make_payment(self):
        db.session.add(self)
        db.session.commit()

    # Used to update that a payment has been successful
    def update_paid_bool(self):
        self.paid = True
        db.session.commit()

    # Updates the cost of a payment
    def update_cost(self, cost):
        self.payment_amount = cost
        db.session.commit()

    # Checks if any payment records have not been paid after 10 minutes
    # and removes the booking and payment record from the database
    @staticmethod
    def payment_timeout():

        # Retrieves a list of payments not yet paid
        unpaid = Payment.query.filter(Payment.paid == False).all()

        if len(unpaid) > 0:
            # Iterates through the list and calculates the time elapsed from creation
            for record in unpaid:
                time_elapsed = datetime.datetime.now() - record.time_created
                time_elapsed = abs(time_elapsed)
                time_elapsed = time_elapsed.total_seconds()

                # Removes the booking and payment record if time elapsed is at least
                # 10 minutes
                if time_elapsed > 600 or time_elapsed == 600:
                    Payment.query.filter_by(id = record.id).delete()
                    Booking.query.filter_by(id = record.booking).delete()

            db.session.commit()

    # Returns a Payment object given an id
    @staticmethod
    def retrieve_payment_record(id):
        return Payment.query.filter(Payment.id == id).first()

    # Function to return the amount of income per location given a week number
    @staticmethod
    def get_weekly_income(week):
        income = {}
        locations = Location.query.all()

        # Initialises the dictionary with income of zero initially
        for location in locations:
            income[location.name] = 0.0

        # Iterates through each booking and adds the sum to the total if the
        # date is in the given week at the corresponding location
        bookings = Booking.query.all()
        for booking in bookings:

            iso_week = booking.start_date_time.isocalendar()[1]

            if iso_week == week:
                # Payment details
                pay_record = Payment.query.filter(Payment.booking == booking.id).first()

                # Booking details
                booking_loc_id = (booking.bicycles[0]).location_id
                booking_loc = Location.query.filter(Location.id == booking_loc_id).first()

                # Location income weekly sum
                current_sum = income[booking_loc.name]
                if pay_record.paid == True:
                    current_sum += pay_record.payment_amount
                    income[booking_loc.name] = current_sum

        return income

    # Initialises an instance of Payment
    def __init__(self,booking,amount,paid):
        self.booking = booking
        self.payment_amount = amount
        self.paid = paid
        self.time_created = datetime.datetime.now()

    # Displayed when printing the object
    def __repr__(self):
        if self.payment_amount is not None:
            return "Amount Paid: £{0}".format(self.payment_amount)
        return "Amount Paid: £0.00"

class Receipt(db.Model):
    __tablename__ = 'receipts'

    #Making the table attributes
    id = db.Column(db.Integer, primary_key = True)
    qr_number = db.Column(db.Integer, nullable = False)
    valid = db.Column(db.Boolean, nullable = False)

    #Adds receipt to table
    def add_receipt(self):
        db.session.add(self)
        db.session.commit()

    #Checks if the receipt ID already exists in the database
    def check_exist(receipt_id):
        exist = Receipt.query.filter_by(qr_number = receipt_id).count()

        if(exist == 0):
            return True
        else:
            return False

    #Initialises an instance of Receipt
    def __init__(self,number,valid):
        self.qr_number = number
        self.valid = valid
