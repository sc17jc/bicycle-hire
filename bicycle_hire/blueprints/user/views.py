from flask import (Blueprint,
                   render_template,
                   flash,
                   url_for,
                   redirect,
                   request,
                   abort)
from flask_login import (login_user,
                         logout_user,
                         current_user,
                         login_required
                        )
from bicycle_hire.blueprints.user.forms import SignupForm, SigninForm
from bicycle_hire.blueprints.user.models import User
from bicycle_hire.blueprints.user.helpers.form_validation import flash_errors
from bicycle_hire.blueprints.user.helpers.url_redirect import is_safe_url

users = Blueprint('user', __name__, template_folder='templates', url_prefix='/account')

@users.route('/signup', methods=['GET', 'POST'])
def signup():
    # If user is already signed in
    if current_user.is_authenticated:
        flash("You are already signed in!", "info")
        return redirect(url_for('bicycles.index'))

    form = SignupForm()

    if form.validate_on_submit():
        new_user = User(form.email.data,
                        form.username.data,
                        form.password.data)
        new_user.db_commit()
        flash("Signed up successfully!", category="success")
        return redirect(url_for('user.signin'))
    else:
        flash_errors(form)
    
    return render_template('user/signup.html', form=form)

@users.route('/signin', methods=['GET', 'POST'])
def signin():
    # If user is already signed in
    if current_user.is_authenticated:
        flash("You are already signed in!", "info")
        return redirect(url_for('bicycles.index'))

    form = SigninForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()

        if user and login_user(user):
            flash("Signed in successfully!", category="success")

            # Get the next url that the user wants to access 
            next_page = request.args.get('next')
            form_id = request.args.get('form_id')
            # Handle the case when there's no next page
            if not next_page:
                return redirect(url_for('bicycles.index'))
            
            # Handle security concerns when following the next user url
            if not is_safe_url(next_page):
                return abort(400)
            
            return redirect(url_for(next_page, form_id=form_id))
    else:
        flash_errors(form)
    
    return render_template('user/signin.html', form=form)


@users.route('/signout')
@login_required
def signout():
    logout_user()
    flash('You have been signed out!', "success")
    return redirect(url_for('user.signin'))
