from wtforms import ValidationError
from bicycle_hire.blueprints.user.models import User
from flask import flash


# Custom username uniqueness validator
def unique_username(message='Username is already taken!'):
    def _unique_username(form, field):
        if User.query.filter_by(username = field.data).first():
            raise ValidationError(message=message)
    return _unique_username


# Custom email uniqueness validator
def unique_email(message='Email is already taken!'):
    def _unique_email(form, field):
        if User.query.filter_by(email = field.data).first():
            raise ValidationError(message=message)
    return _unique_email


# Validates the given password for signing in
def match_password(message='Incorrect username or password!'):
    def _match_password(form, field):
        if not User.check_user(form.username.data, field.data):
            raise ValidationError(message=message)
    return _match_password


# Flash the form validation errors
def flash_errors(form, ctgry='error'):
    for field, errors in form.errors.items():
        for error in errors:
            flash(error, category=ctgry)

    