from flask_login import current_user
from flask import abort
from functools import wraps

# Decorator for the views that require staff authorization
def staff_login_required():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.is_staff():
                return abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator