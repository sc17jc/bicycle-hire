from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo
from bicycle_hire.blueprints.user.helpers.form_validation import unique_username, unique_email, match_password


class SignupForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email(), unique_email()])
    username = StringField('Username', validators=[DataRequired(), unique_username()])
    password = PasswordField('Password', validators=[DataRequired(),
                EqualTo('c_password', message="Passwords don't match!")])
    c_password = PasswordField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('Sign Up')


class SigninForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), match_password()])
    submit = SubmitField('Sign In')
