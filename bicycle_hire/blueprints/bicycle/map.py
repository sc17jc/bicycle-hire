from flask_googlemaps import Map
import datetime, time
from bicycle_hire.blueprints.bicycle.models import Location, Booking, Bicycle

def make_map(start=None, end=None):
    # Number of bikes per location
    no_of_bikes = {}

    # Dictionary of all the locations and their coordinates
    coords = Location.loc_coords()
    
    # Get the number of available bikes per location, given a timeframe
    # If no timeframe, get the number of all the bicycles per location 
    if start and end:
        for loc in coords:
            no_of_bikes[loc] = len(Booking.available_bikes(loc, start, end))
    else:
        no_of_bikes = Bicycle.bicycles_by_location_count()
    
    # Create the Google map
    bike_map = Map(identifier='bike_map',
                   lat=54.237933, lng=-2.36967,zoom=5,
                   style='height:400px;width:100%',
                   markers = [(coords[loc][0],
                               coords[loc][1],
                               loc + ": " + str(no_of_bikes[loc])
                               + " bicycles available") for loc in coords],
                   varname='bike_map',
                   fit_markers_to_bounds=False)

    return bike_map
