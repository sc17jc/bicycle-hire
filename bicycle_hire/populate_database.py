from bicycle_hire.blueprints.bicycle.models import Bicycle, Location, Booking
from bicycle_hire.blueprints.user.models import User
from bicycle_hire.blueprints.payment.models import Payment, Receipt
from bicycle_hire.blueprints.user.models import Roles
from random import randint, random
from bicycle_hire.extensions import db
import datetime, random


def populate():

    # Initialise the database (will clear all your tables!!!)
    db.drop_all()
    db.create_all()

    populateLocation()
    populateBicycle()
    populateBooking()
    populateUsers()
    populatePayment()
    # populateReceipt()


def populateLocation():

    # Location names
    name = {
            0: "London",
            1: "Manchester",
            2: "Leeds",
            3: "Lancaster",
            4: "Birmingham"
           }

    coords = {
              0: [51.509865,-0.118092],
              1: [53.483959,-2.244644],
              2: [53.801277,-1.548567],
              3: [54.04649, -2.79988],
              4: [52.489471,-1.898575]
             }

    # Locations
    for i in range(5):
        db.session.add(Location(coords[i][0], coords[i][1], name[i]))

    # Commit to the database
    db.session.commit()

def populateBooking():

    yesterday = datetime.date.today() - datetime.timedelta(1)

    #Bookings
    for i in range(1,10):
        email = 'email{0}@email.com'.format(i)
        booking = Booking(datetime.datetime.combine(datetime.date.today(),datetime.time(hour=9)),
                        datetime.datetime.combine(datetime.date.today(),datetime.time(hour=17)),
                        email)
        booking.bicycles.append(Bicycle.query.filter(Bicycle.id == i).first())
        db.session.add(booking)

    for i in range(10,20):
        email = 'email{0}@email.com'.format(i)
        booking = Booking(datetime.datetime.combine(yesterday,datetime.time(hour=11)),
                        datetime.datetime.combine(datetime.datetime.today(),datetime.time(hour=11)),
                        email)
        booking.bicycles.append(Bicycle.query.filter(Bicycle.id == i).first())
        db.session.add(booking)

    # Commit to the database
    db.session.commit()


def populateBicycle():

    # Bicycles
    for i in range(500):
        db.session.add(Bicycle(randint(1, 5)))

    # Commit to the database
    db.session.commit()


def populateUsers():

    # Create different User instances
    for i in range(50):
        email = "test{0}@email.com".format(i)
        username = "test{0}".format(i)
        db.session.add(User(email, username, "password"))

    # Create an admin user
    db.session.add(User("admin@admin.com", "admin", "pass", Roles.STAFF))

    # Commit to the database
    db.session.commit()


def populatePayment():

    # Create different payments
    for i in range(1,20):
        booking = i
        db.session.add(Payment(booking, 105.50,True))

    # Commit to database
    db.session.commit()

def populateReceipt():

    # Create receipt instances
    for i in range(1,5):
        qr_number = random.randint(1, 5000)
        db.session.add(Receipt(qr_number, True))

    #Commit to database
    db.session.commit()
